# README #

Completed tasks during 2017 Pentalog internship.

### TASKS ###

* Display the first K matrices of a Latin Square for a given N;
* Write the rule(s) in Pascal's triangle formation numbers;
* Display the range of the maximum sum of a given vector (one dimension array);
* Hanoi Towers;