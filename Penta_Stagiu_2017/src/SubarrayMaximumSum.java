	/* Problem_no_3
	 * 
	 * Please display the range of the maximum sum of a given vector (one dimension array).
	 * (The sum of contiguous subarray within a one-dimensional array of numbers which has the largest sum) 
	 */

import java.util.concurrent.ThreadLocalRandom;
import javax.swing.JOptionPane;

public class SubarrayMaximumSum {
	
	// define "size" as vector number of elements
	// define an array named "vector" and allocate initial memory for 10 integers;
	
	int size;
	int[] vector;
	
	SubarrayMaximumSum(){
		getVectorSize();
		vector = new int[size];
		generateRandomValues();
		printVector();
	}

	// "getVectorSize" will get vector size from user; 
	
	public int getVectorSize(){
		int input = Integer.parseInt(JOptionPane.showInputDialog("Please enter the number of elements for the vector:"));
			JOptionPane.showMessageDialog(null, "The number of elements is " + input + ".");
			size = input - 1;
			return input;
		}
	
	// "generateRandomValues()" will generate random integers from -9 to 9 in the array;
		
	public void generateRandomValues(){
		for(int i = 0; i < size; i++){
			vector[i] = ThreadLocalRandom.current().nextInt(-9, 9);
		}
	}
	
	// "printVector()" will display vector elements;

	public void printVector(){
		System.out.println("Vector's elements displayed below:");
		for(int i = 0; i < size; i++){
			System.out.print("| " + vector[i] + " ");
		}
		System.out.print("|");
		System.out.println("\n");
	}

	// "maxSum()" returns the contiguous subarray within vector which has the largest sum using the Kadane's algorithm;
	
	public int maxSum(){
		int max_so_far = Integer.MIN_VALUE;
		int max_ending_here = 0;
		
		System.out.println("The contiguous subarray which has the largest sum is:");
		
    for (int i = 0; i < size; i++){
        max_ending_here = max_ending_here + vector[i];
        if (max_so_far < max_ending_here){
        	max_so_far = max_ending_here;
        	
        	// displaying the contiguous subarray with the largest sum;
        	System.out.print("| "+ vector[i] + " ");
        }
        if (max_ending_here < 0) {
        	max_ending_here = 0;
        }
    }
    System.out.print(" |");
    System.out.println("\n");
	System.out.println("Maximum sum is: " + max_so_far);
    return max_so_far;
    }

	public static void main(String[] args) {  	// TODO Auto-generated method stub
		SubarrayMaximumSum object1 = new SubarrayMaximumSum();
		object1.maxSum();

	}

}
