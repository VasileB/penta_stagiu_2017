	/* Problem_no_1
	 * 
	 * Please display the first K matrices of a Latin Square for a given N.
	 * Def.: A Latin Square is an NxN matrix.
	 * Each field of the matrix is an element of a set of symbols,
	 * each occurring exactly once in each row and exactly once in each column.
	 * Ex.: A B C / B C A / C A B
	 */

import javax.swing.JOptionPane;

	public class LatinSquare{
		char[][] latSquare;
		int side;
		char[] values;
		int k;
		
	LatinSquare(){
		getInputSize();
		latSquare = new char[side][side];
	}

	// I'm asking the user to provide the side of the latin square;
	// The square values will be chosen from 26 alphabet letters, so the side must be less than or equal to 26;
	
	public int getInputSize(){
		int inputSide = Integer.parseInt(JOptionPane.showInputDialog("Please enter the side of the Latin Square."
				+ "\n (integer value, less than or equal to 26)"));
		JOptionPane.showMessageDialog(null, "The side of the Latin Square is: " + inputSide);
		side = inputSide;
		return inputSide;
	}
	
	// "values" is an array that will contain all 26 alphabet letters;
	
	public char[] getValues(){
		values = new char[26];
		int x = 0;
		for(int i = 0; i < 26; i++){
			values[i] = (char)(97 + (x++));
		}
		return values;
	}
	
	// "getLatSquare()" will create an array "latSquare" with the user defined side;
	// the IF loop allows each letter to only occur once in each row and once in each column;
	
	public char[][] getLatSquare(){
		for(int i = 0; i < side; i++){
			for(int j = 0; j < side; j++){
				if((i+j) < side){
					latSquare[i][j] = values[i+j];
				}
				else {
					latSquare[i][j] = values[i+j-side];
				}
				}
		}
		return latSquare;
	}

	public void displayLatSquare(){
		System.out.println("\nThe Latin Square with side = " + side + " is displayed below:\n");
		for(int i = 0; i < latSquare.length; i++){
			for(int j = 0; j < latSquare.length; j++){
				System.out.print("| " + latSquare[i][j] + " ");
			}
			System.out.println("|");
		}
	}	

	// "getInputK()" requires the user to provide the first k values to be displayed from the square;	
	
	public int getInputK(){		
		int inputK = Integer.parseInt(JOptionPane.showInputDialog("Please enter the number of the first K matrices to display"));
		JOptionPane.showMessageDialog(null, "The first " + inputK + " matrices will be displayed.");
		k = inputK;
		return inputK;		
	}
	
	// then these first k matrices are displayed;  
	
	public void displayKMatrices(){
		System.out.println("\nThe matrix with first " + k + " values is displayed below:\n");
		for(int i = 0; i < k; i++){
			for(int j = 0; j < k; j++){
				System.out.print("| " + latSquare[i][j] + " ");
			}
			System.out.println("|");
		}
	}
	
	public static void main(String[] args) {  // TODO Auto-generated method stub
		LatinSquare object1 = new LatinSquare();
		object1.getValues();
		object1.getLatSquare();
		object1.displayLatSquare();
		
		object1.getInputK();
		object1.displayKMatrices();
				
	}  

}
