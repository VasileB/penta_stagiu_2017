import javax.swing.JOptionPane;

/* Problem_no_2
	 * 
	 * Please write the rule(s) in Pascal's triangle formation numbers.
	 * Ex.: 
	 * 1
	 * 1 1
	 * 1 2 1
	 * 1 3 3 1
	 * 1 4 6 4 1
	 * 1 5 10 10 5 1
	 * 1 6 15 20 15 6 1
	 * ...
	 */

	public class Pascal_Triangle {
		int noOfRows;
	
		Pascal_Triangle(){
			noOfRows = 6;
		}
	
	//	"getTriangleSize()" gets the number of rows from user;
		
	public int getTriangleSize(){
		int input = Integer.parseInt(JOptionPane.showInputDialog("Please enter the number of rows\nfor the Pascal's Triangle."));
			JOptionPane.showMessageDialog(null, "The number of rows is " + input + ".");
			noOfRows = input - 1;
			return input;
		}

	// "getPascalTriangle()" generates the triangle with two FOR loops; 
	
	public void getPascalTriangle(){
		int a, b;
		System.out.println("\nPascal's Triangle generated with FOR loops:");
		for(int i = 0; i <= noOfRows; i++){
			a = 1;
			b = i+1;
			
			// this FOR loop gets the empty space at the left side of the triangle;
			
			for(int j = noOfRows - i; j > 0; j--){   
				System.out.print(" ");
			}
			
			// this FOR loop gets the values for the Pascal triangle;			
			
			for(int x = 0; x <= i; x++){	
				if (x > 0) {
					a = a * (b - x) / x;
				}
				System.out.print(a + " ");
			}
			System.out.println();
		}
	}
	
	// "getArrayTriangle()" generates the triangle with an Array[][];	
	
	public void getArrayTriangle(){
		noOfRows++;
	    int[][] pascal  = new int[noOfRows + 1][];
	    pascal[1] = new int[1 + 2];
	    pascal[1][1] = 1;
	    
	    System.out.println("\nPascal's Triangle generated with an Array[][]:");
	    
	    for (int i = 2; i <= noOfRows; i++) {
	        pascal[i] = new int[i + 2];
	        
	        for (int j = 1; j < pascal[i].length - 1; j++) {
	            pascal[i][j] = pascal[i-1][j-1] + pascal[i-1][j];
	            }
	    }
	    
	    for (int i = 1; i <= noOfRows; i++) {
	    	
	        for (int j = 1; j < pascal[i].length - 1; j++) {
	            System.out.print(pascal[i][j] + " ");
	        }
	        System.out.println();
	    }
	}
	
	
	public static void main(String[] args) {          // TODO Auto-generated method stub
		Pascal_Triangle object1 = new Pascal_Triangle();
		object1.getTriangleSize();
		object1.getPascalTriangle();
		object1.getArrayTriangle();
		
	}

}
